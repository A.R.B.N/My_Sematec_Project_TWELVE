package ir.arbn.www.mysematecprojecttwelve;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by A.R.B.N on 2/19/2018.
 */

public class IncomingCallReciver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, ".:Incoming Call:.", Toast.LENGTH_SHORT).show();

    }
}
